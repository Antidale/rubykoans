#!/usr/bin/env ruby -wKU

require './dice_set.rb'

class Player
	attr_reader :score
	attr_reader :name
	attr_reader :dice
	attr_reader :stash
	attr_reader :continue

	def initialize(initial_name)
      @name = initial_name.capitalize
      @score = 0
      @dice = DiceSet.new
      @stash = "stash"
      @continue = "continue"
	end

	def add_to_score (score_to_add)
		@score += score_to_add
	end

	def continue_or_stash
		playerResponse = ""

		until (playerResponse == :continue.to_s || playerResponse == :stash.to_s)
			puts ("#{@name}, do you want to continue or stash your score? (continue, stash)")
			playerResponse = gets.chomp.downcase
		end

		return playerResponse
	end

	def roll_dice
		if @dice.values.nil? || @dice.values.length == 0
			return @dice.roll(5)
		else
			return @dice.roll(@dice.values.length)
		end
	end

	def reset_dice(dice = nil)
		@dice.values = if dice.nil? then [] else dice end
	end

end 