require File.expand_path(File.dirname(__FILE__) + '/neo')

require './dice_set.rb'
require './player.rb'
require './greed_game.rb'
require './scoring_helper.rb'


## TODO list: 
# => figure out why the remove method has an issue with evaluating die == 5 for the delete when there are sets
# 	=> see scoring_helper.remove_scored_dice for
class PlayerTests < Neo::Koan

	def test_player_initialize_sets_name
		player = Player.new("Ethel")
		assert_equal "Ethel", player.name
	end

	def test_player_initialize_capitalizes_name
		player = Player.new("fred")
		assert_equal "Fred", player.name
	end

	def test_player_initialize_sets_score_to_zero
		player = Player.new("Ethel")
		assert_equal 0, player.score
	end

	def test_add_to_score_updates_score
		player = Player.new("Fred")

		player.add_to_score(5)

		assert_equal 5, player.score
	end

	def test_add_to_score_when_accumulates_score_totals
		player = Player.new("Ethel")

		assert_not_nil player
		player.add_to_score(300)
		player.add_to_score(5)

		assert_equal 305, player.score
	end

	def test_reset_dice_sets_to_empty_array_when_arg_is_nil
		player = Player.new("Watson")
		player.reset_dice

		assert_equal [], player.dice.values
	end

	def test_reset_dice_sets_to_passed_in_array
		player = Player.new("Watson")
		player.reset_dice [1,3,4]

		assert_equal [1,3,4], player.dice.values
	end

	def test_roll_dice_rolls_five_dice_when_dice_values_is_nil
		player = Player.new("Watson")
		player.roll_dice
		
		assert_equal 5, player.dice.values.length
	end

	def test_roll_dice_rolls_five_dice_when_dice_values_length_is_zero
		player = Player.new("Watson")
		player.reset_dice
		player.roll_dice
		
		assert_equal 5, player.dice.values.length
	end

	def test_roll_dice_rolls_dice_values_length_when_more_than_zero_dice
		player = Player.new("Watson")
		
		player.reset_dice [7,7,7]
		player.roll_dice 
		
		assert_equal 3, player.dice.values.length
		assert_not_equal [7,7,7], player.dice.values
	end
end

class ScoringHelperTests < Neo::Koan

	def test_remove_scored_dice_removes_ones
		scoring = ScoringHelper.new
		array = [1,2,3,4,1,2]
		returnedArray = scoring.remove_scored_dice array 

		assert_equal [2,2,3,4], returnedArray
	end

	def test_remove_scored_dice_removes_fives
		scoring = ScoringHelper.new
		array = [5,2,3,4,5,2,5,5]
		returnedArray = scoring.remove_scored_dice array 

		assert_equal [2,2,3,4], returnedArray
	end

	def test_remove_scored_dice_sorts_array
		scoring = ScoringHelper.new
		array = [6,4,3,2,3,10]
		returnedArray = scoring.remove_scored_dice array 

		assert_equal [2,3,3,4,6,10], returnedArray
	end

	def test_remove_scored_dice_removes_sets_of_three
		scoring = ScoringHelper.new
		array = [2,2,2,2]
		returnedArray = scoring.remove_scored_dice array 

		assert_equal [2], returnedArray
	end

	def test_remove_scored_dice_removes_multiple_sets_of_three
		scoring = ScoringHelper.new
		array = [2,2,2, 2,2,2, 2,2]
		returnedArray = scoring.remove_scored_dice array 

		assert_equal [2,2], returnedArray
	end

	def test_remove_scored_dice_removes_multiple_numbers_with_sets_of_three
		scoring = ScoringHelper.new
		array = [2,2,2, 3,3,3, 4,4]
		returnedArray = scoring.remove_scored_dice array 

		assert_equal [4,4], returnedArray
	end	

	def test_remove_scored_dice_removes_set_and_single_one
		scoring = ScoringHelper.new
		array = [2,3,3,3,1]
		returnedArray = scoring.remove_scored_dice array 

		assert_equal [2], returnedArray
	end

	def test_remove_scored_dice_removes_set_and_single_five
		scoring = ScoringHelper.new
		array = [2,3,3,3,5]
		returnedArray = scoring.remove_scored_dice array 

		assert_equal [2], returnedArray
	end
end

class GameTests < Neo::Koan

	#initialize tests
	def test_game_initialize_sets_players_array_to_empty_when_no_argument_passed_in
		game = Game.new

		assert_equal [], game.players
	end

	def test_game_initialize_sets_players_array_to_array_arg
		ethel = Player.new("Ethel")
		fred = Player.new("Fred")
		players = [ethel, fred]

		game = Game.new(players)
		assert_equal players, game.players
	end

	def test_game_initialize_sets_in_the_game_to_300_as_default
		
		game = Game.new()
		assert_equal 300, game.in_the_game_score
	end

	def test_game_initialize_sets_final_round_score_to_3000_as_default
		
		game = Game.new()
		assert_equal 3000, game.final_round_score
	end

	def test_game_can_score_turn_returns_false_when_current_score_eq_0_and_added_score_less_than_300
		player = Player.new("Clara")
		game = Game.new([player])

		returnValue = game.can_score_turn(player, 299)
		
		assert_equal false, returnValue
	end

	def test_game_can_score_turn_returns_false_when_current_score_eq_1_and_added_score_less_than_300
		player = Player.new("Clara")
		game = Game.new([player])

		player.add_to_score(1)
		returnValue = game.can_score_turn(player, 299)
		
		assert_equal false, returnValue
	end

	def test_game_can_score_turn_returns_true_when_current_score_eq_0_and_added_score_eq_300
		player = Player.new("Clara")
		game = Game.new([player])

		returnValue = game.can_score_turn(player, 300)
		
		assert_equal true, returnValue
	end

	def test_game_can_score_turn_returns_true_when_current_score_eq_300_and_added_score_eq_300
		player = Player.new("Clara")
		game = Game.new([player])

		player.add_to_score(1)
		returnValue = game.can_score_turn(player, 300)
		
		assert_equal true, returnValue
	end

	def test_game_can_score_turn_returns_true_when_current_score_eq_300_and_added_score_less_than_300
		player = Player.new("Clara")
		game = Game.new([player])
		player.add_to_score(300)
		returnValue = game.can_score_turn(player, 1)
		
		assert_equal true, returnValue
	end


	def test_display_scores_does_not_change_players_turn_order

		ethel = Player.new("Ethel")
		fred = Player.new("Fred")
		players = [ethel, fred]
		ethel.add_to_score(50)
		fred.add_to_score(100)
		game = Game.new(players)

		game.display_scores

		assert_equal players, game.players
	end

	def test_display_scores_does_not_change_players_turn_order

		ethel = Player.new("Ethel")
		fred = Player.new("Fred")
		players = [ethel, fred]
		ethel.add_to_score(50)
		fred.add_to_score(100)
		game = Game.new(players)
		ordered_players = [fred, ethel]	
		
		returned_value = game.display_scores

		assert_equal ordered_players, returned_value
	end

	def test_current_player_sets_first_player_in_array_as_starting_player

		ethel = Player.new("Ethel")
		fred = Player.new("Fred")
		players = [ethel, fred]

		game = Game.new(players)
		
		first_player = game.current_player

		assert_equal ethel, first_player
	end

	def test_current_player_returns_correct_player_on_turn_gt_players_length

		ethel = Player.new("Ethel")
		fred = Player.new("Fred")
		amy = Player.new("Amy")
		players = [ethel, fred, amy]

		game = Game.new(players)
		game.current_turn = 5
		current_player = game.current_player

		assert_equal amy, current_player
	end
		
end

class DiceSetTests < Neo::Koan

	def test_roll_dice_returns_the_rolls_in_an_array
		dice = DiceSet.new
		rolled_dice = dice.roll(5)

		assert rolled_dice.is_a?(Array)
	end

	def test_roll_dice_return_value_is_the_values_array
		dice = DiceSet.new
		rolled_dice = dice.roll(5)

		assert_equal rolled_dice, dice.values
	end

	def test_can_create_a_dice_set
    dice = DiceSet.new
    assert_not_nil dice
  end

  def test_rolling_the_dice_returns_a_set_of_integers_between_1_and_6
    dice = DiceSet.new

    dice.roll(5)
    assert dice.values.is_a?(Array), "should be an array"
    assert_equal 5, dice.values.size
    dice.values.each do |value|
      assert value >= 1 && value <= 6, "value #{value} must be between 1 and 6"
    end
  end

  def test_dice_values_do_not_change_unless_explicitly_rolled
    dice = DiceSet.new
    dice.roll(5)
    first_time = dice.values
    second_time = dice.values
    assert_equal first_time, second_time
  end

  def test_dice_values_should_change_between_rolls
    dice = DiceSet.new

    dice.roll(5)
    first_time = dice.values

    dice.roll(5)
    second_time = dice.values

    assert_not_equal first_time, second_time,
      "Two rolls should not be equal"
  end

  def test_you_can_roll_different_numbers_of_dice
    dice = DiceSet.new

    dice.roll(3)
    assert_equal 3, dice.values.size

    dice.roll(1)
    assert_equal 1, dice.values.size
  end

end


class IntegrationTests < Neo::Koan
	def test_reset_dice_and_scoring_helper_remove_scored_dice_integrate_correctly
		player = Player.new("Watson")
		scoring_helper = ScoringHelper.new
		player.reset_dice (scoring_helper.remove_scored_dice ([1,3,4]))

		assert_equal [3,4], player.dice.values
	end

end