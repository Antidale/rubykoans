class DiceSet
  attr_accessor :values

  def roll (diceToRoll)
    @values = []
    diceToRoll.times { @values << rand(1..6) }
    return @values
  end
end