#!/usr/bin/env ruby -wKU

require './scoring_helper.rb'

class Game
	attr_reader :players
	attr_accessor :current_turn
	attr_reader :in_the_game_score
	attr_reader :final_round_score

	def initialize(players = [])
		@players = players
		@current_turn = 0
		@scoring_helper = ScoringHelper.new
		@in_the_game_score = 300
		@final_round_score = in_the_game_score * 10
	end

	def take_turn(player)
		lastRollPoints = 0
		pointsThisTurn = 0
		playerChoice = ""
		puts("\n#{player.name} it is your turn\n")
		display_scores
		player.reset_dice
		
		until (playerChoice == player.stash)
			lastRollPoints = @scoring_helper.score(player.roll_dice)
			pointsThisTurn += lastRollPoints 
			
			if lastRollPoints == 0
				puts("#{player.name} lost to greed and scored 0 points this round")
				break
			end

			puts "#{player.name} scored #{lastRollPoints} this throw on #{player.dice.values}"
			puts "and has accumulated #{pointsThisTurn} this turn"
			
			player.reset_dice (@scoring_helper.remove_scored_dice(player.dice.values))

			playerChoice = player.continue_or_stash
		end

		if (playerChoice == player.stash)
			player.add_to_score(pointsThisTurn) if can_score_turn(player, pointsThisTurn)
			puts "#{player.name} ends their turn with #{player.score} points"
		end
	end

	def can_score_turn(player, turn_score)
		return player.score >= @in_the_game_score || turn_score >= @in_the_game_score
	end

	def get_players
		numberOfPlayers = ""
		until (numberOfPlayers.to_i > 0)
			puts("How many players are playing?")
			numberOfPlayers = gets.chomp.to_i
		end

		numberOfPlayers.times do |i|
			puts("What is the name of player #{i + 1}?")
			@players << Player.new(gets.chomp)
		end
	end

	def play_game		
		get_players if number_of_players < 1

		until @players.any? { |player| player.score >= 3000 }
			take_turn(current_player)
			
			@current_turn += 1
		end

		puts "Final Round"
		(number_of_players - 1).times do 
			take_turn(current_player)
		end

		display_scores
	end

	def current_player
		return @players[@current_turn % players.length]
	end

	def number_of_players
		return @players.length
	end

	def display_scores
		playersArray = @players.sort { |a, b| b.score <=> a.score }
		playersArray.each do |player|
			puts ("#{player.name} has #{player.score} points")
		end
		return playersArray #for tests, because I don't want to deal with reading from stdin quite yet
	end
end