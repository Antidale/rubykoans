# Triangle Project Code.

# Triangle analyzes the lengths of the sides of a triangle
# (represented by a, b and c) and returns the type of triangle.
#
# It returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal
#
# The tests for this method can be found in
#   about_triangle_project.rb
# and
#   about_triangle_project_2.rb
#
def triangle(a, b, c)
	sidesArray = [a,b,c].sort
	#sides must be geater than 0
	raise TriangleError if sidesArray.min <= 0
	#the smallest two sides should still be bigger than the third when added together
	raise TriangleError if sidesArray[0] + sidesArray[1] <= sidesArray[2]
	#this is essentially myArrah[myIndex] where we're looking at the length/count of unique sides
	[:nil, :equilateral, :isosceles, :scalene][sidesArray.uniq.length]
end

# Error class used in part 2.  No need to change this code.
class TriangleError < StandardError
end
