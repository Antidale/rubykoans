class ScoringHelper

	def score(dice)
	  counterHash = Hash.new(0)
	  totalScore = 0;

	  dice.each do | die | 
	    counterHash[die] += 1
	  end

	  counterHash.each do | die, count |
	    individualDiePoints = 0
	    
	    setOfThreePoints = if count / 3 > 0 then die * 100 else 0 end
	    
	    if die == 1
	      setOfThreePoints *= 10 #sets of 1s are worth 1000, so 100 * 10
	      individualDiePoints = (count % 3) * 100  #1s outside of sets are worth 100 each
	    elsif die == 5
	      individualDiePoints = (count % 3) * 50 # 5s outside of sets are worth 50 each
	    end
	       
	    totalScore += setOfThreePoints + individualDiePoints
	  end

	  return totalScore
	end

	def remove_scored_dice(diceArray)
		dice = diceArray.sort
		dice.delete(1)
		dice.delete(5) 
		
		dice.each do |die|
			#why does this not remove fives when there's a set of dice to slice out, too?
			#dice.delete(die) if die == 1
			#dice.delete(die) if die == 5
			dice.slice!(dice.find_index(die), 3) if dice.count(die) > 2
		end
		
		return dice
	end
end